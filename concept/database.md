# 数据

## 基本数据类型

狮偶将基本数据类型分为几种类型  
![基本类型](img/database/基本类型.png)  
在传递数值的过程中，同一种数据类型只能传递给相同类型的状态机、状态变量或局部变量。

## 自定义数据类型

有些数值并不是单个出现，如二维坐标，我们可以根据需要在数据结构中设置。当需要一次性返回多个数值时，可以通过自定义数据类型实现。  
![坐标](img/database/坐标.png)

## 添加数据类型方法

狮偶支持数据库导入，导入文件为Excel表格
本文将介绍如何实现数据库功能。以[数据可视化DEMO](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22proj%22:%22/testing/sample/03/life-expectancy/project.json%22%7D)为例。

### 一、创建数据结构

 1、新建一个数据结构组并打开  
![新建一个数据结构组](img/database/新建数据结构组.png)

### 二、添加字段

 
![设置字段](img/database/设置字段.png)
 - 数据类型名对应Excel表格的sheets，见图用红色方框圈起来的部分
 
 - Excel表格中sheet命名格式要求为：模块名.数据类型名（见图用红色方框圈起来的部分）
 
 - 蓝色矩形圈起来的部分为数据剧类型中字段名称
 
 - ❗❗❗在Excel各列单元格格式要与<font color=green>基本类型</font>对应，否将将导致导入数据失败。
 
 - 映射元素类不需要在Excel表格填写，配合id自动在数据库生成数组。


![Excel表格](img/database/Excel表格1.png)




## 内置数据库

内置数据库是狮偶语言内置的只读数据库。

数据与程序分别控制的好处是，按照约定写出的数据，可以与程序分别修改。

比如游戏的运行机制、逻辑、规则是代码，游戏的关卡设置是数据。这样就可以通过一套代码，配合数据的调整，开发出很多的关卡。

再比如，线上营销活动是个逻辑，是代码，但是活动的内容是可以变更的，可以数据。就像每年购物节，商品打折的逻辑是一样的，但是打折的商品和折扣是不一样的。就可以通过变更数据来达到复用一个功能、不修改代码的目的。



### 1.定义数据结构并关联数据内容

为了方便与其他系统交换数据，狮偶使用Excel格式（xlsx后缀）导入数据。可以用Office、wps或者任何支持保存xlsx文件的软件生成。

为了导入Excel数据，要建立与数据内容匹配的数据结构。

我们可以先建立数据结构，然后再制作匹配的Excel表格。也可以根据已有的Excel表格，制作数据结构。

这里，我们假定已有Excel表格，根据Excel表格设立数据结构。

如，在“展示信息”模块里建立一个新的“学校信息”数据结构组。

![新建数据结构组](img/database/新建数据结构.png)

模块中的数据结构组，是为了便于我们组织代码，提高可读性和可维护性，可以将业务关联度较高的数据结构放在同一个组里。

然后我们可以在数据结构组里建立数据结构。

需要从Excel导入的数据结构，第一个属性名称必须是id，且必须是整数类型。

例如：

![整数ID](img/database/%E6%95%B4%E6%95%B0ID.png)

对应的，在Excel中，需要为每种数据结构建立一张表格，表格的名称格式为：

[模块名称].[数据结构名称]

如：


![表格名称](img/database/%E8%A1%A8%E6%A0%BC%E5%90%8D%E7%A7%B0.png)



文件中可以包含多个表格，每个表的名称要与对数据结构的全名一致，且表格第一列、数据结构的第一个属性，必须是小写字母id。

Excel表格的第一行，表示列的名字。
为了便于程序匹配，狮偶要求，Excel的表第一列，也必须是id，且这一列的内容必须是整数。

如：
![ID列](img/database/id%E5%88%97.png)

>id列的内容不可重复。
>id列的内容可以不连续，但是从1开始连续递增，对我们编写程序提供很大便利。所以尽量从1开始，逐次递增。


先写数据结构或者先写Excel都可以。如果我们已经有Excel文件，也可以先导入到狮偶里。

![导入Excel](img/database/%E5%AF%BC%E5%85%A5Excel.png)

因为刚刚我们已经建立了数据结构，导入成功后就可以看到“班级”结构的数据已经列出来了：

![导入成功](img/database/%E5%AF%BC%E5%85%A5%E6%88%90%E5%8A%9F.png)

点击“展示信息.班级”，打开数据视图：

![班级信息](img/database/%E7%8F%AD%E7%BA%A7%E4%BF%A1%E6%81%AF.png)

这里列出了数据结构中已经设定的属性。

>狮偶支持多选导入，以便于不同的表格可以分别放在多个Excel文件里，但是导入的时候必须能够多选、一次性导入，所以应该放在同一个文件夹里。

>多次导入，即使包含不同的表格，每次导入的表格也被视为替换之前所有的内容。

我们可以在数据结构设计中添加更多的内容来把其他的列也识别到。

![更多属性](img/database/%E6%9B%B4%E5%A4%9A%E5%B1%9E%E6%80%A7.png)

在数据视图就可以看到完整的属性：

![班级完整信息](img/database/%E7%8F%AD%E7%BA%A7%E5%AE%8C%E6%95%B4%E4%BF%A1%E6%81%AF.png)

同样我们添加学生数据结构：
![班级和学生数据结构](img/database/%E7%8F%AD%E7%BA%A7%E5%92%8C%E5%AD%A6%E7%94%9F%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84.png)

![两个数据表](img/database/%E4%B8%A4%E4%B8%AA%E6%95%B0%E6%8D%AE%E8%A1%A8.png)

![学生信息](img/database/%E5%AD%A6%E7%94%9F%E4%BF%A1%E6%81%AF.png)

### 2.获取数据

当我们可以在数据视图中看到所有我们需要的列的数据，我们就可以开始编写程序去读取这些数据了。

每一个Excel表格导入进来之后狮偶会依据数据结构的设定对数据重新组织，形成数据集，所有的数据集组成了数据库。

通过数据集信息的块，我们就可以很容易的通过id获取到数据集中的数据。

![数据集块](img/database/%E6%95%B0%E6%8D%AE%E9%9B%86%E5%9D%97.png)

例如
获取班级信息：
![获取班级信息](img/database/%E8%8E%B7%E5%8F%96%E7%8F%AD%E7%BA%A7%E4%BF%A1%E6%81%AF.png)
获取学生信息
![获取学生信息](img/database/%E8%8E%B7%E5%8F%96%E5%AD%A6%E7%94%9F%E4%BF%A1%E6%81%AF.png)

### 下载原始数据

最终导入的Excel文件会保留在工程中，如果需要导出原始文件，可以下载完整工程并解压缩出 data 文件夹查看。

### 参考样例

[学生信息展示](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22srczip%22:%22/testing/sample/06/studisplay/%E5%AD%A6%E7%94%9F%E4%BF%A1%E6%81%AF%E5%B1%95%E7%A4%BA.obp%22%7D)

[数据可视化](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22proj%22:%22/testing/sample/03/life-expectancy/project.json%22%7D)

[气候变化可视化](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22proj%22:%22/testing/sample/03/ecovisuals/project.json%22%7D)

[人生重启模拟器](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22srczip%22:%22/testing/sample/07/liferestart/%E4%BA%BA%E7%94%9F%E9%87%8D%E5%90%AF%E6%A8%A1%E6%8B%9F%E5%99%A8.obp%22%7D)

