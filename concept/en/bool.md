# Boolean

## Boolean Operation Example

Use code to determine if the values of x and y are equal.

## Case Practice
### 【1】Setting Local Variables

1. Drag the “Start” button from the “Events” section to the programming area:

2. Drag two “Set Local Variable () Initialize Value()” from “State Variables” and connect them to “Start”

3. Drag the “Integer()” module from “Math” and connect it to the back of the local variable creation module

4. Change the variable names to x and y, and set the integer number to 1 for now

![1](../img/bool/1.bmp)

Explanation

Here, x and y are variables that will be used as parameters in subsequent judgment operations. The initialization value represents setting the values of x and y to the set integer when the program starts.

### 【2】Setting Judgment Conditions

1. Drag the “If · Execute” block from “Boolean” and connect it below the set local variable module

Module Explanation: The “If · Then” block means that if the condition behind “If” is met, the code below will run.

2. Drag the “[ ] = [ ]” block from “Boolean” and connect it to the back of “If”

3. Drag two “Local Variable[]” from “Local Variables” and place them in the two blank spaces of “[ ] = [ ]” respectively, and modify the local variable names to x and y

4. Click the gear icon in the top left corner of the “If · Execute” block, and in the popup window, move the “Else” on the left to below “If”

![alt](../img/bool/2.bmp)

Note: Step 4 is for output when the condition is not met.

### 【3】Writing Code for When the Condition is True and False

![alt](../img/bool/3.bmp)

Explanation of Text Input Code: [Explanation of Text Input Code](../img/bool/4.bmp)

### 【4】Debugging

1. Adjust the initial value of x to 2 and the value of y to 1, check if the output result meets expectations

2. Adjust the initial value of x to 1 and the value of y to 1, check if the output result meets expectations

Source Code:
![alt](../img/bool/5.bmp)