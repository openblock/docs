# Variables

| Type | Scope |
| ---- | ---- |
|State Machine Variable|	Valid at any location within the state machine where it is created|
|State Variable|	Valid at any location within the state where it is created|
|Local Variable|	Valid from the location of creation to the end of its containing area|

## Adding Variables

> State machine variables and state variables must be added in the left menu bar to be used.

## Knowledge Points

In OpenBlock, we divide variables into 3 scopes: state machine, state, and local. 

The variables within these three scopes are referred to as: state machine variables, state variables, and local variables.

State Machine Variable: This refers to a variable that can be read and written within any state of the state machine.

State Variable: This refers to a variable that can be read and written anywhere within the current state. In any function within the function group, you cannot write state machine variables or state variables. State machines also cannot read or write each other’s state machine variables or state variables. Between different states within the same state machine, variables cannot be read or written to each other either.

Local Variable: More complex [as shown in the following figure:]

![图片3](../img/variableWorkspace/图片3.png)

The scope of a local variable starts from the “Create Local Variable” block and ends at the end of its enclosing statement block.

![图片4](../img/variableWorkspace/图片4.png)

For example, the country variable is a local variable, and its scope starts from “Create local variable ‘country’” and ends at the lower limit of the loop enclosure.