# Data

## Basic Data Types

OpenBlock categorizes basic data types into several types
![基本类型](../img/database/基本类型.png)  
When passing values, only variables of the same data type can be passed to state machines, state variables, or local variables.

## Custom Data Types

Some values do not occur alone, such as two-dimensional coordinates, which we can set as needed in the data structure. When multiple values need to be returned at once, this can be achieved through custom data types.
![坐标](../img/database/坐标.png)

## Adding Data Types Method

OpenBlock supports database import, with the import file being an Excel spreadsheet. This article will explain how to implement database functionality. We will use the [Data Visualization DEMO](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22proj%22:%22/testing/sample/03/life-expectancy/project.json%22%7D) as an example.

### 1. Create Data Structure

Create a new data structure group and open it
![新建一个数据结构组](../img/database/新建数据结构组.png)

### 2. Add Fields


![设置字段](../img/database/设置字段.png)
- The data type name corresponds to the sheets in the Excel spreadsheet, as indicated by the red box
    
- The naming format for sheets in the Excel spreadsheet is required to be: [module name].[data type name] (as indicated by the red box)
- The blue rectangle outlines the field names in the data structure
- ❗❗❗The cell format in each column of the Excel spreadsheet must correspond to the basic types, otherwise the import will fail.
- Mapping elements do not need to be filled in the Excel spreadsheet; they are automatically generated in the database with the corresponding id.

![Excel表格](../img/database/Excel表格1.png)


## Built-in Database

The built-in database is a read-only database included in the OpenBlock language.

The benefit of separating data and program control is that data written according to conventions can be modified separately from the program.

For example, the gameplay mechanics, logic, and rules are code, while the level settings are data. This allows for many levels to be developed with a single set of code, by adjusting the data.

Similarly, online marketing campaigns have a logic, which is code, but the content of the campaign can be changed, making it data. Just like during annual shopping festivals, the logic of product discounts is the same, but the discounted products and discounts are different. The purpose of reusing a function without modifying the code can be achieved by changing the data.

### 1. Define Data Structures and Associate Data Content

To facilitate data exchange with other systems, OpenBlock uses Excel format (with an xlsx extension) to import data. This can be generated using Office, WPS, or any software that supports saving files in xlsx format.

To import Excel data, a data structure that matches the content of the data must be established.

You can either create the data structure first and then produce a matching Excel spreadsheet, or create the data structure based on an existing Excel spreadsheet. 

Here, we assume there is an existing Excel spreadsheet and create the data structure based on it.

For example, in the “Display Information” module, create a new “School Information” data structure group.

![新建数据结构组](../img/database/新建数据结构.png)

The data structure groups within the module are organized to improve code readability and maintainability, and data structures with high business relevance can be placed in the same group.

Then, we can establish data structures within the data structure group.

For data structures to be imported from Excel, the first attribute name must be id, and it must be of integer type.

For example:

![整数ID](../img/database/%E6%95%B4%E6%95%B0ID.png)

Correspondingly, in Excel, a separate table must be created for each data structure, with the table name formatted as:

[Module Name].[Data Structure Name]

For example:

![表格名称](../img/database/%E8%A1%A8%E6%A0%BC%E5%90%8D%E7%A7%B0.png)

The file can contain multiple tables, and each table’s name must match the full name of the data structure, with the first column of the table and the first attribute of the data structure being id in lowercase.

The first row of the Excel table represents the names of the columns. For ease of program matching, OpenBlock requires that the first column of the Excel table also be id, and the content of this column must be integers.

For example:
![ID列](../img/database/id%E5%88%97.png)

>The content of the id column must not be repeated. 
>The content of the id column can be non-continuous, but starting from 1 and continuously increasing provides great convenience for us to write programs. So it’s best to start from 1 and increment sequentially.

Whether you write the data structure first or the Excel spreadsheet, it doesn’t matter. If we already have an Excel file, we can import it into OpenBlock first.

![导入Excel](../img/database/%E5%AF%BC%E5%85%A5Excel.png)

Since we’ve already established the data structure, after a successful import, you can see the data for the “Class” structure listed:

![导入成功](../img/database/%E5%AF%BC%E5%85%A5%E6%88%90%E5%8A%9F.png)

Click on “Display Information.Class” to open the data view:

![班级信息](../img/database/%E7%8F%AD%E7%BA%A7%E4%BF%A1%E6%81%AF.png)

Here, all the attributes that have been set in the data structure are listed.

>OpenBlock supports multi-selection import, so different tables can be placed in multiple Excel files, but they must be in the same folder to allow for multi-selection and one-time import during the import process.

>Multiple imports, even if they include different tables, are considered to replace all previous content each time.

We can add more content to the data structure design to recognize other columns as well.

![更多属性](../img/database/%E6%9B%B4%E5%A4%9A%E5%B1%9E%E6%80%A7.png)

In the data view, you can see the complete attributes:

![班级完整信息](../img/database/%E7%8F%AD%E7%BA%A7%E5%AE%8C%E6%95%B4%E4%BF%A1%E6%81%AF.png)

Similarly, we add the student data structure: 
![班级和学生数据结构](../img/database/%E7%8F%AD%E7%BA%A7%E5%92%8C%E5%AD%A6%E7%94%9F%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84.png)

![两个数据表](../img/database/%E4%B8%A4%E4%B8%AA%E6%95%B0%E6%8D%AE%E8%A1%A8.png)

![学生信息](../img/database/%E5%AD%A6%E7%94%9F%E4%BF%A1%E6%81%AF.png)


### 2. Getting Data

Once we can see all the columns of data we need in the data view, we can start writing programs to read this data.

Every time an Excel table is imported, OpenBlock reorganizes the data according to the settings of the data structure, forming a dataset, and all datasets together constitute the database.

With the data set information block, we can easily get the data from the dataset by id.

![数据集块](../img/database/%E6%95%B0%E6%8D%AE%E9%9B%86%E5%9D%97.png)

For example, to get class information:  
![获取班级信息](../img/database/%E8%8E%B7%E5%8F%96%E7%8F%AD%E7%BA%A7%E4%BF%A1%E6%81%AF.png)
To get student information: 
![获取学生信息](../img/database/%E8%8E%B7%E5%8F%96%E5%AD%A6%E7%94%9F%E4%BF%A1%E6%81%AF.png)

### Downloading Raw Data

The final imported Excel file is retained within the project. If you need to export the original file, you can download the complete project and extract the data folder to view.

### Reference Examples

[学生信息展示](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22srczip%22:%22/testing/sample/06/studisplay/%E5%AD%A6%E7%94%9F%E4%BF%A1%E6%81%AF%E5%B1%95%E7%A4%BA.obp%22%7D)

[数据可视化](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22proj%22:%22/testing/sample/03/life-expectancy/project.json%22%7D)

[气候变化可视化](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22proj%22:%22/testing/sample/03/ecovisuals/project.json%22%7D)

[人生重启模拟器](https://mlzone.areyeshot.com/openblock/frontpage/index.html#%7B%22srczip%22:%22/testing/sample/07/liferestart/%E4%BA%BA%E7%94%9F%E9%87%8D%E5%90%AF%E6%A8%A1%E6%8B%9F%E5%99%A8.obp%22%7D)