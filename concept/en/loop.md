# Loop

## Loop Block Group Position

Enter the status interface, and open the loop in the left menu bar.

![在左侧菜单栏中点开循环](../img/loop/capture_20220114192109679.jpg)

After opening, you can see that there are four modules in this list.

## Explanation of Loop Blocks

### 1. Repeat [ ] times

![重复执行](../img/loop/capture_20220114194050374.jpg)

Explanation: Do the same thing N times, such as repeating it 10 times.

### 2. Conditional Repeat

This block has a drop-down menu with two options: “Repeat while condition is met” and “Repeat until condition is met.”

**Repeat while condition is met:**

![模块讲解](../img/loop/capture_20220114194454784.jpg)

When the logic of the block connected to the right of “Repeat while condition is met” is satisfied, the code inside the “Do” block will be executed, similar to the while() in other programming languages.

**Repeat until condition is met:**

![模块讲解](../img/loop/capture_20220114195230911.jpg)

Explanation: This block means to repeatedly execute the code until a certain condition is met, at which point the loop ends. Similar to other programming languages using while(1){ if(condition){ statement; break;} }

### 3. Variable counts from () to (), increasing by () each time

![拖块讲解](../img/loop/capture_20220114195429048.jpg)

This block means to use a variable i to count starting from a certain number, and each time the code inside the loop body is executed, i increases by a certain amount. When the value reaches a certain number, the loop ends.

For example, in the image, the variable i starts counting from 1, and each time the code inside the loop body is executed, i increases by 1. When i increases to 10, the loop ends. Similar to a for loop in other programming languages.

4. Break out of loop / Proceed to next iteration

![跳出循环](../img/loop/capture_20220114195755127.jpg)

**Break out of loop:**

Exit the loop body and no longer execute the code inside the loop.

**Proceed to next iteration:**

Skip the code blocks below “Proceed to next iteration” in the loop body and directly enter the next iteration of the loop.