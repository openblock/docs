# Making the “Jump and Jump” Game

This article will use the OpenBlock language to implement the basic functionality of the WeChat mini-game “Jump and Jump”.

The original game is built on the WeChat mini-game platform, utilizing the 3D capabilities provided by the platform. To focus on the core process of game design and production, we have simplified the development content and implemented all the business of “Jump and Jump” in a 2D environment. To simplify development, we use circles and rectangles to represent characters and wireframe cubes to represent platforms, creating a 2.5D effect.

![平台简化效果](img/jump/平台.png)
![简化效果](img/jump/简化效果.png)

## Business Analysis

### Operation Flow

- After players enter the game, they first see the “Start Game” interface. By clicking the “Start Game” button, they enter the game scene. When the player falls off the platform, the score for that round is displayed, along with a “Play Again” button. Players can re-enter the game scene by clicking the “Play Again” button. Scores do not accumulate.
- The “Jump” effect is to accept the player’s screen touch event, start timing, and make the character jump when the finger is lifted off the screen. The distance of the jump is determined by the length of time the player touches the screen. The shorter the touch time, the shorter the jump distance; the longer the time, the longer the distance. However, there is a maximum time limit for touching, and the timing stops accumulating once this limit is reached. The jump height is fixed.

### Game Rules

#### Basic Concepts:

- Users participating in the game are called players.
- The jumping object controlled by the player is called the character.
- The block that the character can stand on is called a first-level platform.

#### Game Process:

- The game is defined as a round starting from the character standing, compressing, jumping, and landing. At the beginning of each round, two platforms are drawn on the screen, placed at a 45-degree angle to the left front or right front.
- At the beginning of each round, the view is moved so that the center of the view is between the platform the character is standing on and the next level platform.
- Players can only see the platforms they have jumped over, the current standing platform, and the next level platform. Each time a platform is jumped, the next platform appears randomly to the left front or right front, and is randomly positioned within a range of the current platform. Since the following platforms are not visible, the character cannot jump two platforms at once.
- In the first round, the platform the character is standing on is the first level, and the second level platform is to the right front. Starting from the third level platform, the direction is random.
- The character, controlled by the player, jumps from the nearby platform to the forward platform.
- When the player presses the screen (or other interaction method), the platform and character compress simultaneously. When they are compressed to the maximum value, the compression stops.
- Whether or not the maximum compression is reached, when the player’s finger leaves the screen, the platform and character bounce up simultaneously.
- After bouncing to the maximum value, the character jumps up and towards the next platform. The distance is calculated based on the compression value.
After the character lands, the score for the current round is determined. If the character lands on the platform from which they jumped, no points are scored, and the game continues. If the character lands on the next level platform, points are scored, and the game continues. If the character does not land on a platform, they are deemed to have fallen off the platform, and the game ends.

### Technical Design

#### Function Breakdown

By analyzing the game rules, we can conclude that the game requires five functional modules: UI, coordinates, platforms, characters, and user interaction.
- The UI module is responsible for showing the game status and score to the player.
- Using coordinates, we can simulate the effect of moving the camera and easily change the field of view.
- Platforms are calculated for position within our own coordinate system, rather than the screen’s coordinate system, to facilitate the displacement of platforms.
- By placing characters and platforms in the same coordinate system for calculation, we can easily determine whether the character’s landing point is on the platform.
- The user interaction module is used to receive operations such as screen presses and pass these operations to modules like characters and platforms.
- To simplify calculations and support future possibly irregularly shaped platforms, no matter where the starting point is on the previous level, if the landing point is not on the previous level platform, it must be on the straight line connecting the midpoint between the previous and next level platforms.

#### Module Design

- Game Control       
    - Controls the outermost logic of the game, such as launching the homepage and displaying the score page when the game ends.
- Coordinate System       
    - The coordinate system for “Jump and Jump” is quite simple; it’s not even a system but merely adding a coordinate offset to each content that needs to be drawn.
- Platform       
    - A simplified platform just needs to draw a variable-height cube effect based on the coordinates. The height changes according to the duration the player touches the screen.
- Character       
    - The character module is similar to the platform module.
- User Interaction       
    - User interaction is simplified to screen touches. No button effects are designed. The interaction system is quite simple and can be directly implemented using an event system.
- UI       
    - The UI only needs to display the score and the “Start Game” and “Play Again” text.


#### Character
      
Based on our previous design of the character, we can further break down the character’s functions. First, we distinguish between the logic and drawing of the character.       
According to the user’s operation flow, we can break down the character into five states: waiting, compressing, bouncing, falling, and landing. Each state requires the character to be drawn. We can summarize that in these five states, there are only two differences in drawing the character: position and compression. The position represents the character’s offset from the top left corner of the screen in x and y coordinates, while compression represents the deformation of the character during the compression and bouncing processes. We can create a state machine and set up five states for the character.     
![角色状态](img/jump/角色状态.png)
For drawing, we only need to draw simple circles and rectangles on the screen based on the x-coordinate, y-coordinate, and height. Therefore, we define a function to draw the character that requires x-coordinate, y-coordinate, and height as parameters.       
Drawing is relatively simple and does not depend on any state, so we can create a “Drawing” function group outside the state machine. Within this group, we create the “Draw Character” function: 
![绘制角色](img/jump/绘制角色.png)

#### Platform

Compared to the character, the platform only has two states: compressed and not compressed, with relatively simple logic. 
![平台状态](img/jump/平台状态.png)
The platform is represented as a cube, which is complex to draw. Since it belongs to the drawing function, we add the “Draw Platform” function to the “Drawing” function group. 
![绘制平台1](img/jump/绘制平台1.png)

(omitted middle parts…)

![绘制平台2](img/jump/绘制平台2.png)

Here, only the beginning and end parts are shown. The general idea is to draw each line segment separately, then draw the top surface. The order is not important.       
The “Drawing” function group now has two functions: 
![绘制函数组](img/jump/绘制函数组.png)
In the Start.Main state machine’s initial state, add the test code for drawing the platform and character. The running effect: 
![运行效果](img/jump/运行效果.png)

#### Game Control
Game control is the module that governs the overall macro state of the game, dividing the game into three states: launch, game process, and end. The game is in the launch state when it starts; it enters the game process state when the screen is clicked; it enters the end state when the character falls off the platform; and it restarts the game process when the screen is clicked again. 
![游戏控制状态](img/jump/游戏控制状态.png)

#### UI
      
The UI changes according to the game state. In the launch, process, and end states, different interface structures are displayed. Since the scoring logic is very simple, we also write the scoring function in the UI.

Overall code structure:
![总览](img/jump/总览.png)

## Code Implementation

### Game Control

In the Main state machine, add the logic to create a “Game Control” state machine to guide the game’s startup. 
![Main](img/jump/Main.png)

#### Startup
      
The logic for starting up is quite simple; it only requires creating a UI state machine and listening for a click event. 
![游戏控制-启动](img/jump/游戏控制-启动.png)

#### Game Process - 1
      
The game process involves initialization and rendering refresh, which is a complex area. However, we won’t consider all the details just yet. Instead, we’ll build the general framework for game control and fill in the detailed logic later. For now, we just need to create a character and broadcast a “Game Start” message. At the same time, we need to listen for the “Fell Off Platform” message to enter the end state when the character falls off the platform. 
![游戏过程-1](img/jump/游戏过程-1.png)

#### End

After the game ends, simply listen for the player’s screen tap and then return to the game process to restart the game. 
![游戏结束](img/jump/游戏结束.png)

#### Check State Transition

We have completed the framework for the game control module. Let’s verify our design by checking the state transitions. 
![查看状态转换](img/jump/查看状态转换.png)

![游戏控制状态转换](img/jump/游戏控制状态转换.png)

From the diagram, we can see that the game indeed follows our design, transitioning from startup to the game process, to the end, and then back to the game process, indicating that there are no issues with the flow.

### Startup
      
When the game starts, the UI simply needs to prompt “Tap the screen to start.” 
![UI启动](img/jump/UI启动.png)

### Game Process

#### UI 1
      
Once the game starts, the UI begins to draw the current score in the top left corner of the screen.  
![UI游戏过程](img/jump/UI游戏过程.png)
We can see that the current score is displayed in the top left corner. However, the “Tap the screen to start” message still remains on the screen.

#### Game Process 2
      
Since the drawing canvas does not automatically clear, each new content drawn will be superimposed on the old content, and the old content will not disappear. Therefore, we need to actively clear the content that is no longer needed. We can add a signal to redraw in the game control module. 
![游戏控制-2](img/jump/游戏控制-2.png)

Starting from the “Game Process” state, send a “Redraw” message to itself every 100 milliseconds. Upon receiving this message, clear all content on the screen and broadcast the “Refresh UI” message.

#### UI 2
Now that we have the “Refresh UI” message, we can receive it in the UI.        
![UI游戏过程-2](img/jump/UI游戏过程-2.png)
Run the game again to see that the issue of the score and “Tap the screen to start” appearing simultaneously is no longer present.

#### Character Initialization
      
After creating the character, we should set an initial position for it. If we set the initial position in the “Waiting” state, the character will be reset to the initial position every time it returns to the “Waiting” state. Therefore, we can consider giving the character an “Initialization” state. We add an “Initialization” state to the character and set it as the default state. 
![角色状态2](img/jump/角色状态2.png)

![角色初始化](img/jump/角色初始化.png)


#### Offset Coordinates
      
Like the UI, in order to draw the character, we also set a render frame rate for it. However, this frame rate is different from the UI’s. The UI frame rate does not need to consider the world coordinate system. But to simplify the calculation of the position of the graphics to be drawn, each character and platform in the game content is in our set virtual space coordinate system, not the screen coordinate system. We need to use a simple position offset to describe this, and when drawing graphics, we calculate the offset into the coordinates.

First, we create a data structure group.
![创建数据结构](img/jump/创建数据结构.png)
Then, within this group, we create the structure for the coordinates. 
![坐标结构](img/jump/坐标结构.png)

#### Game Process 3
      
With the coordinate system in place, we can move the entire world using a unified offset.