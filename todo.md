# 未展示的细节
## pdf展示

 <script src="assets/js/pdfobject.min.js"></script>
```pdf
../files/pdf/GOTC-OpenBlock.pdf
```
```pdf
<object height="700" width="950" border="0" data="GOTC-OpenBlock.pdf" type="application/pdf">
    <embed src="../files/pdf/GOTC-OpenBlock.pdf" type="application/pdf" width=800 height=800 />
</object>
```
在index.html配置
 //md setting
    markdown: {
      code: function(code, lang){ 
        var renderer_func = function(code, lang, base=null) { 
	        var pdf_renderer = function(code, lang, verify) {
		      function unique_id_generator(){
			      function rand_gen(){
				      return Math.floor((Math.random()+1) * 65536).toString(16).substring(1);
			      }
			      return rand_gen() + rand_gen() + '-' + rand_gen() + '-' + rand_gen() + '-' + rand_gen() + '-' + rand_gen() + rand_gen() + rand_gen();
		      }
		      if(lang && !lang.localeCompare('pdf', 'en', {sensitivity: 'base'})){
			      if(verify){
				      return true;
			      }else{
				      var divId = "markdown_code_pdf_container_" + unique_id_generator().toString();
				      var container_list = new Array();
				      if(localStorage.getItem('pdf_container_list')){
					      container_list = JSON.parse(localStorage.getItem('pdf_container_list'));	
				      }
				      container_list.push({"pdf_location": code, "div_id": divId});
				      localStorage.setItem('pdf_container_list', JSON.stringify(container_list));
				      return ('<div style="margin-top:'+ PDF_MARGIN_TOP +'; margin-bottom:'+ PDF_MARGIN_BOTTOM +';" id="'+ divId +'">'+ '<a href="'+ code + '"> Link </a> to ' + code +'</div>');
			      } 
		      }
		      return false;
	      }
	      if(pdf_renderer(code, lang, true)){
	        return pdf_renderer(code, lang, false);
	      }
	     
	        return (base ? base : this.origin.code.apply(this, arguments));
        }
      }
    },


## 3d模型展示
```
<link href="assets/css/docsify-iframe.min.css">
<script src="assets/js/docsify-iframe.min.js"></script>
<script src="assets/js/gltfexplorer.min.js"></script>
```
使用方法（去掉斜杆）
```gltf
    \\```gltf
        \\../files/3d/Fox.gltf
    \\```
```
去掉响应头X-Content-Type-Options: nosniff