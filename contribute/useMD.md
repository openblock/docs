# 狮偶文档书写技巧
> 此方法仅在狮偶文档站点适用

##  多标签
<!-- tabs:start -->

#### **English**
Hello!
#### **French**
Bonjour!
#### **Italian**
Ciao!
<!-- tabs:end -->

```
<!-- tabs:start -->

#### **English**

Hello!

#### **French**

Bonjour!

#### **Italian**

Ciao!

<!-- tabs:end -->
```


<!-- tabs:start -->
#### **Bold**
展示
```
假设这是代码66666666666
```
#### **<em>Italic</em>**
让我放个图
![编辑器界面](../start/img/pic1.png)

#### **<span style="color: red;">Red</span>**
懒得写了

#### **:smile:**
...

#### **😀**
...

#### **Badge <span class="tab-badge">New!</span>**
...
<!-- tabs:end -->

![源码](img/useMD/pic1.png)

## 警告样式

> [!NOTE]
> note使用样例

```
> [!NOTE]
> note使用样例
```
> [!NOTE|style:flat|label:自定义标题|iconVisibility:hidden]
> 带有flat样式的note警告类型 。我要凑字数凑字数凑字数凑字数凑字数。
> 注意这个警告隐藏了特定图标且可以自定义标题。

```
> [!NOTE|style:flat|label:自定义标题|iconVisibility:hidden]
> 带有flat样式的note警告类型 。我要凑字数凑字数凑字数凑字数凑字数。
> 注意这个警告隐藏了特定图标且可以自定义标题。
```

> [!TIP]
> 提示使用样例

```
> [!TIP]
> 提示使用样例
```
> [!TIP|style:flat|label:自定义标题|iconVisibility:hidden]
> 带有flat样式的TIP警告类型 。不凑字数了。
> 注意这个警告隐藏了特定图标且可以自定义标题。

```
> [!TIP|style:flat|label:自定义标题|iconVisibility:hidden]
> 带有flat样式的TIP警告类型 。不凑字数了。
> 注意这个警告隐藏了特定图标且可以自定义标题。
```

> [!WARNING]
> 警告使用样例

```
> [!WARNING]
> 警告使用样例
```
> [!WARNING|style:flat|label:自定义标题|iconVisibility:hidden]
> 带有flat样式的WARNING警告类型 。不凑字数了。
> 注意这个警告隐藏了特定图标且可以自定义标题。

```
> [!WARNING|style:flat|label:自定义标题|iconVisibility:hidden]
> 带有flat样式的WARNING警告类型 。不凑字数了。
> 注意这个警告隐藏了特定图标且可以自定义标题。
```

> [!ATTENTION]
> 注意使用样例

```
> [!ATTENTION]
> 注意使用样例
```
> [!ATTENTION|style:flat|label:来点英文吧|iconVisibility:hidden]
> An alert of type 'tip' using alert specific style 'flat'.
> In addition, this alert uses an own heading and hides specific icon.

```
> [!ATTENTION|style:flat|label:来点英文吧|iconVisibility:hidden]
> An alert of type 'tip' using alert specific style 'flat'.
> In addition, this alert uses an own heading and hides specific icon.
```

## 控制播放GIF
> 1.鼠标悬停在图片上时播放(默认)


![样例1](../start/img/pic6.gif)
```
![样例1](../start/img/pic6.gif)
```

> 2.鼠标点击图片播放，再次点击停止播放


![样例2](../start/img/pic3.gif "-gifcontrol-mode=click;")
```
![样例2](../start/img/pic3.gif "-gifcontrol-mode=click;")
```
> 3.自定义样式


![样例3](../start/img/pic3.gif "-gifcontrol-iconColor=#ff0000; -gifcontrol-overlayColor=rgba(0,0,0,0.7); -gifcontrol-playIcon=<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path d=\"M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z\"/></svg>;")
```
![样例3](../start/img/pic3.gif "-gifcontrol-iconColor=#ff0000; -gifcontrol-overlayColor=rgba(0,0,0,0.7); -gifcontrol-playIcon=<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path d=\"M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z\"/></svg>;")
```





