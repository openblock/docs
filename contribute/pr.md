# 贡献作品至官网
## 工具
1. **Gitee账号**
> [Gitee](https://Gitee.com)(码云) 是  OSCHINA.NET 推出的代码托管平台,支持 Git 和 SVN,提供免费的私有仓库托管。
狮偶开源源码托管于此平台,向狮偶提交PR需要注册[Gitee](Gitee.com)账号。

Gitee官网:[Gitee.com](https://Gitee.com)



2. **Git**
>Gitee是基于Git的平台，所以我们需要下载[Git](https://git-scm.com/)

git官网:[git-scm.com](https://git-scm.com/)


> [!ATTENTION]
> 此外，我们希望您在使用Git前花10分钟了解一下关于git的常识，[点击观看](https://www.bilibili.com/video/BV1PG411L7of/?spm_id_from=333.337.search-card.all.click&vd_source=63d7323884dcbf6031b8870810dffd48)。教程提到的命令看看就行，至于为什么您看一下下一个工具的描述。

3. **vscode**
> vscode是优秀的编辑器，集成了git图形化。有了vscode,我们不需要死记git命令。


vscode官网：[code.visualstudio.com](https://code.visualstudio.com/)

## Fork仓库
> 进入狮偶开源作品仓库网址：[gitee.com/openblock/testing](https://gitee.com/openblock/testing)，点击右上角的fork


![点击fork](img/pr/fork.png)

> 选择fork到自己的仓库上


![选择](img/pr/fork1.png)

> fork到自己的地盘后，进入自己的仓库复制地址


![复制代码](img/pr/copy.png)
## 克隆仓库
> 打开vscode编辑器点击左侧菜单栏的源代码管理器，如果没有出现该图标。按快捷键ctrl+shift+g

![源代码管理器](img/pr/git.png)

> 克隆仓库，粘贴地址后按回车。

![粘贴](img/pr/paste.png)
![选择桌面](img/pr/chosse.png)
## 本地修改
> 打开资源管理器

![资源管理器](img/pr/zy.png)

>在根目录按照自己项目名新建一个文件夹。比如项目名叫"木头人",可以新建一个名为"woodboy"的文件夹，该文件夹用于放置自己的工程。

![新建文件夹](img/pr/newfile.png)
> 回到编辑器导出完整工程并将该工程放置在自己新建的文件夹内。

> 选择一张合适的样例封面一并放到文件夹内。
> 打开根目录下的`list.json`，插入以下键值对到数组中，格式如下


```json
{
    "name": "helloworld",
    "url": "helloworld/helloworld.obp",
    "img": "helloworld/helloworld.png"
}, 
```
> [!WARNING|style:flat|label:自定义标题|iconVisibility:hidden]
>name是项目名，url是项目地址,
>img是项目封面图片地址

## 提交修改
> 点击源代码管理器

![源代码管理器](img/pr/c.png)

> 点击更改右侧的加号

![添加](img/pr/add.png)

> 在消息栏处填写“添加XXX样例"。XXX是你的项目名称。然后提交


![消息](img/pr/me.png)
> 点击菜单栏上的终端选择"新建终端"

![zd](img/pr/zd.png)

>在终端分别输入以下两句命令

将freedom替换成你的名字

```
git config --global user.name "freedom"
```
将123@qq.com替换成你的邮箱
```
git config --global user.email "123@qq.com"

```

![ts](img/pr/ts.png)

> 在弹出的窗口填写你的gitee账号和密码，然后点击确定

## 新建PR

回到自己的gitee仓库仓库

点击新建Pull Request
![](img/pr/pr.png)

标题依旧是添加XXX样例，内容简单写写

提交完后记得签署协议。