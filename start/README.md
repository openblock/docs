# 快速上手

> 😁狮偶不需要安装任何软件，二次开发者可跳转至[安装](diy/install.md)教程。
## 打开在线编辑器

在线编辑器地址：[mlzone.areyeshot.com/openblock/frontpage/](/)

![编辑器界面](img/pic1.png)

## 微信扫一扫登录

❗❗❗第一次登录的用户必须用微信扫一扫，登录后即可绑定用户名与密码。
![登录](img/pic2.png)

## 新建模块
![新建模块](img/pic3.gif "-gifcontrol-mode=click;")

## 新建状态机
![新建状态机](img/pic4.png)

## 点击进入状态
![进入状态](img/pic5.png)

## 显示一段文字
![新建模块](img/pic6.gif "-gifcontrol-mode=click;")
