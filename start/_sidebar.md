<!-- docs/start/_sidebar.md -->
**入门**
* [简介](/)
* [快速上手](start/)

**基础**
* [变量](concept/variableWorkspace.md)
* [布尔](concept/bool.md)
* [循环](concept/loop.md)
* [数据](concept/database.md)

**物联网**
* [大师兄](iot/)

**案例**
* [跳一跳半成品](catables/jump.md)

**贡献**
* [贡献作品至官网](contribute/pr.md)
* [文档书写技巧](contribute/useMD.md)

**二次开发**
* [安装](diy/install.md)
* [主题](diy/)


**社区治理**