# Quick Start

> 😁 OpenBlock does not require any software installation. Secondary developers can skip to the Installation tutorial.

## Open the Online Editor

Online Editor Address: [mlzone.areyeshot.com/openblock/frontpage/](/)

![编辑器界面](img/pic1.png)

## Log in with WeChat Scan

❗❗❗ Users logging in for the first time must use WeChat to scan and log in. After logging in, you can bind a username and password. 

## Create a New Block
![新建模块](img/pic3.gif "-gifcontrol-mode=click;")

## Create a New State Machine
![新建状态机](img/pic4.png)

## Click to Enter State
![进入状态](img/pic5.png)

## Display a Piece of Text
![新建模块](img/pic6.gif "-gifcontrol-mode=click;")
